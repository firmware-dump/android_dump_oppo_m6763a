exclude="ck\\.fs|pl\\.fs|sysmd5\\.zip|sd_after|install-recovery\\.sh|install_recovery\\.sh|recovery-from-boot\\.p|recovery_rootcheck"
if [ "$#" -eq 2 ];then
pushd $2  2>&1 > /dev/null;
fi
if [ "$1" = "deep" ]; then
md5=`busybox find system -type f | busybox grep -Ev $exclude | busybox sort | busybox xargs busybox md5sum | busybox md5sum | busybox cut -b  -32 `
else
md5=`busybox find system -type f -o -type l | busybox xargs busybox ls -l --color=never | busybox grep -Ev $exclude | busybox awk '{
print $5,$9}' | busybox sort | busybox md5sum | busybox cut -b -32`
fi
build_time=`cat system/build.prop  | busybox grep utc| busybox cut -b  19-`
echo "$build_time,$md5";

if [ "$#" -eq 2 ];then
popd 2>&1 > /dev/null;
fi
