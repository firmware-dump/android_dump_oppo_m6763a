#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:18959264:95c983bdf57f68d924f5afdff0db9272aaddcf86; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/bootdevice/by-name/boot:11652000:ccaafdabeb212d6b1dc344b5266be57af14d6b38 EMMC:/dev/block/platform/bootdevice/by-name/recovery 95c983bdf57f68d924f5afdff0db9272aaddcf86 18959264 ccaafdabeb212d6b1dc344b5266be57af14d6b38:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
